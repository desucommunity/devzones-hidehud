#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <devzones>
#include <multicolors>
#include <influx/hud>

new bool:hide[MAXPLAYERS+1];
new equipo[MAXPLAYERS+1];

#define SHOWFLAG_TIMER	( 0 << 0 )

public Plugin:myinfo =
{
	name = "SM DEV Zones - Hide HUD",
	author = "Franc1sco franug",
	description = "",
	version = "2.0",
	url = "http://www.zeuszombie.com/"
};

public OnPluginStart()
{
	HookEvent("player_spawn", PlayerSpawn);
	HookEvent("player_team", Event_PlayerTeam);
}

public OnClientDisconnect(client)
{
	hide[client] = false;
}

public Event_PlayerTeam(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	equipo[client] = GetEventInt(event, "team");
}

public Action:PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(hide[client]) NoHide(client);
	CreateTimer(4.0, Pasado, client);
}

public Action:Pasado(Handle:timer, any:client)
{
	if(IsClientInGame(client)) 
		if(hide[client]) NoHide(client);
}

public Zone_OnClientEntry(int client, const char [] zone)
{
	if(client < 1 || client > MaxClients || !IsClientInGame(client) ||!IsPlayerAlive(client)) 
		return;
		
	if(StrContains(zone, "hidehud", false) != 0) return;
	
	CPrintToChat(client," {darkred}[HIDE HUD]{lime}You entered in a hide hud zone");
	
	if(!hide[client]) YesHide(client);
	
}

public Zone_OnClientLeave(int client, const char [] zone)
{
	if(client < 1 || client > MaxClients || !IsClientInGame(client) ||!IsPlayerAlive(client)) 
		return;
		
	if(StrContains(zone, "hidehud", false) != 0) return;
	
	CPrintToChat(client," {darkred}[HIDE HUD]{lime}You left the hide hud zone");
	
	if(hide[client]) NoHide(client);

}


NoHide(client)
{
	//SDKUnhook(client, SDKHook_SetTransmit, Hook_SetTransmit);
	Influx_SetClientHideFlags(client, SHOWFLAG_TIMER );
	
	hide[client] = false;
}

YesHide(client)
{
	//SDKHook(client, SDKHook_SetTransmit, Hook_SetTransmit); 
	Influx_SetClientHideFlags(client, HIDEFLAG_TIMER );
	hide[client] = true;
}


